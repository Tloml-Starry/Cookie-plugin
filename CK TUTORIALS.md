# 曲奇点点乐《游玩教程》
1. 初次游玩，请先@机器人发送`点击曲奇`以创建曲奇信息
1. 信息创建完成之后，即可发送`领取收益`领取离线收益了
1. 此离线收益无上限，可一直挂机，直到下次领取
1. 收益达到 **15** 即可购买一份指针，指令`购置建筑指针*1` 指针每秒生产 **0.1** 份曲奇
1. 你也可以选择再攒攒等达到 **100** 之后购买一份老太婆，指令`购置建筑老太婆*1`老太婆每秒生产 **1** 份曲奇
1. 更多可购买建筑可发送指令`建筑商店`来查看
 ---
__大致玩法就是以上内容，新内容更新较慢__