# 曲奇点点乐
[![Gitee](https://img.shields.io/badge/Gitee-曲奇点点乐-black?style=flat-square&logo=gitee)](https://gitee.com/Tloml-Starry/Cookie-plugin/) [![云崽bot](https://img.shields.io/badge/云崽-v3.0.0-black?style=flat-square&logo=dependabot)](https://gitee.com/Le-niao/Yunzai-Bot) [![A-Yunzai](https://img.shields.io/badge/A_Yunzai-Azai_Bot-black?style=flat-square&logo=dependabot)](https://gitee.com/ningmengchongshui/azai-bot) [![Group](https://img.shields.io/badge/群号-392665563-red?style=flat-square&logo=GroupMe&logoColor=white)](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=5YqRg5S_pk_oVolSp7wydrx2ZFblhE-U&authKey=rl1zGbz8KHpvTlgkd%2BDm5Z9y%2FdMaWFnIL9p0FNVS6dyNmkNQ%2F2ngy3oU5Gynx8Ob&noverify=0&group_code=392665563) <a href='https://gitee.com/Tloml-Starry/Cookie-plugin/stargazers'><img src='https://gitee.com/Tloml-Starry/Cookie-plugin/badge/star.svg?theme=dark' alt='star'></img></a>

![动态访问量](https://count.kjchmc.cn/get/@Cookie-plugin?theme=rule34)  
《饼干点点乐》是一款制作饼干，数量大到夸张的小游戏。为了制作更多的饼干，你可以招兵买马各式各样的饼干制造者，比如慈祥的老奶奶，农场，工厂和异世界的穿越门。  
为[Yunzai-Bot V3](https://gitee.com/Le-niao/Yunzai-Bot) & [A-Yunzai](https://gitee.com/ningmengchongshui/azai-bot)提供曲奇点点乐游戏[点我体验网页版](https://orteil.dashnet.org/cookieclicker/)  
本库更新较慢，如出现插件报错或功能不适配等情况请及时联系[作者](https://gitee.com/Tloml-Starry)或发起[issues](https://gitee.com/Tloml-Starry/Cookie-plusin/issues)  
参与贡献请Fock本仓库修改代码后发起[Pull Request](https://gitee.com/Tloml-Starry/Cookie-plugin/pulls)  
如果你对本仓库表示支持，请点点你手中的Star
## 联系我
点开我的主页私信我
 & [点击跳转我的QQ](https://qm.qq.com/q/h9UHpRmomc)
 & [点击跳转我的QQ群](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=5YqRg5S_pk_oVolSp7wydrx2ZFblhE-U&authKey=rl1zGbz8KHpvTlgkd%2BDm5Z9y%2FdMaWFnIL9p0FNVS6dyNmkNQ%2F2ngy3oU5Gynx8Ob&noverify=0&group_code=392665563)

---

## 安装&更新
在Yunzai根目录运行命令
```
git clone https://gitee.com/Tloml-Starry/Cookie-plugin.git ./plugins/Cookie-plugin/
```

如何更新？
 - 使用Yunzai自带指令`#全部更新`
 - 运行`UPDATE.bat`文件，位置在"Yunzai/plugins/Cookie-plugin"内

## 注！

 * **该游戏容易刷屏，不建议在聊天群游玩，请创建专门的游戏群使用**
 
## 功能
<details>
<summary>指令及描述</summary>

| 指令 | 描述 |
| :------: | :-----: |
| 点击曲奇 | 创建用户基础信息，点击增加曲奇 |
| 领取收益 | 领取建筑生产收益 |
| 曲奇统计信息 | 游玩统计信息 |
| 建筑商店 | 查看所有可购买建筑 |
| 购置建筑(建筑)*(数量) | 如`购置建筑指针*1` |
</details>

### 未来计划
* 高优先级
    - [ ] 兑换码
    - [ ] 建筑升级
* 低优先级
    - [ ] 飞升
    - [ ] 天堂商店
    - [ ] 成就系统

## 其他文游
[面包商店(Bread-Shop)](https://gitee.com/Tloml-Starry/Bread-Shop)  
[种田模拟器(Farming-Simulator)](https://gitee.com/Tloml-Starry/Farming-Simulator)  
[群友市场(Slave-Market)](https://gitee.com/Tloml-Starry/Slave-Market)  
[按钮(Button)](https://gitee.com/Tloml-Starry/Button)  