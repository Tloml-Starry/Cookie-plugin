import { GET_DATA, SAVE_DATA, IS_THERE_USER } from '../utils/Gs.js';
import fs from 'fs'

const ALL_USER_FOLDER = 'plugins/Cookie-plugin/data/USER INFO/'
export class COOKIE_PLUGIN extends plugin {
    constructor() {
        super({
            name: '[Cookie]点击曲奇',
            dsc: '点击一下曲奇',
            event: 'message',
            priority: 1,
            rule: [{
                reg: /^(#|\/)?点击曲奇$/,
                fnc: 'CLICK_COOKIE'
            }]
        })
    }

    async CLICK_COOKIE(e) {
        const USER_ID = e.user_id

        const USER_FILE = `${ALL_USER_FOLDER}${USER_ID}.json`
        const USER_NUMBER = (fs.readdirSync(ALL_USER_FOLDER)).length

        if (!IS_THERE_USER(USER_ID)) {
            CREATING_USER_INFORMATION(USER_ID)

            return e.reply((e.adapter === 'QQBot') ? [
                '# 欢迎游玩曲奇点点乐',
                `> 第${USER_NUMBER}位用户`,
                '曲奇点点乐信息创建完成',
                '可发送**领取收益**领取离线收益',
                '如有问题请联系开发者',
                Bot.Button([[
                    { label: '领取收益', enter: true }, { label: '联系开发者', link: 'https://gitee.com/Tloml-Starry/Cookie-plugin' }
                ]])
            ] : [
                segment.at(USER_ID),
                '\n欢迎游玩曲奇点点乐',
                `\n第${USER_NUMBER}位用户`,
                '\n您可发送[领取收益]领取离线收益'
            ])
        }

        const USER_DATA = GET_DATA(USER_FILE)

        const USER_BI = USER_DATA['BI']
        const USER_B_N_ARRAY = [
            USER_BI['指针'], USER_BI['老太婆'], USER_BI['农场'], USER_BI['矿坑'], USER_BI['工厂'],
            USER_BI['银行'], USER_BI['神庙'], USER_BI['巫师塔'], USER_BI['装运飞船'], USER_BI['炼金术实验室'],
            USER_BI['传送门'], USER_BI['时光机'], USER_BI['反物质聚光镜'], USER_BI['棱镜'], USER_BI['机会制造器'],
            USER_BI['分形引擎'], USER_BI['Javascript控制台'], USER_BI['空闲宇宙'], USER_BI['脑皮屑面包师'], USER_BI['你']
        ]

        const USER_ALL_B_N = USER_B_N_ARRAY.reduce((accumulator, currentValue) => accumulator + currentValue, 0);

        let INITIAL_QUANTITY = 1
        // 建筑收益 - 有升级
        if (USER_DATA['UI']['强化食指']) INITIAL_QUANTITY *= 2
        if (USER_DATA['UI']['腕管护理霜']) INITIAL_QUANTITY *= 2
        if (USER_DATA['UI']['左右开弓']) INITIAL_QUANTITY *= 2
        let TFE = 0
        if (USER_DATA['UI']['千手指']) TFE = (USER_ALL_B_N - USER_B_N_ARRAY[0]) * 0.1
        if (USER_DATA['UI']['百万手指']) TFE *= 5
        if (USER_DATA['UI']['十亿手指']) TFE *= 10
        if (USER_DATA['UI']['一兆手指']) TFE *= 20
        if (USER_DATA['UI']['千兆手指']) TFE *= 20
        if (USER_DATA['UI']['一京手指']) TFE *= 20
        if (USER_DATA['UI']['一垓手指']) TFE *= 20
        if (USER_DATA['UI']['一秭手指']) TFE *= 20
        if (USER_DATA['UI']['千秭手指']) TFE *= 20
        if (USER_DATA['UI']['100穰手指']) TFE *= 20
        if (USER_DATA['UI']['十进制手指']) TFE *= 20
        if (USER_DATA['UI']['手指']) TFE *= 20
        INITIAL_QUANTITY += TFE

        USER_DATA['SI']['当前饼干数量'] += INITIAL_QUANTITY
        USER_DATA['SI']['当前总饼干生产数量'] += INITIAL_QUANTITY
        USER_DATA['SI']['总生产饼干数量'] += INITIAL_QUANTITY
        USER_DATA['SI']['点击曲奇次数'] += 1
        USER_DATA['SI']['点击曲奇总产量'] += INITIAL_QUANTITY
        USER_DATA['SI']['点击曲奇产量'] = INITIAL_QUANTITY
        SAVE_DATA(USER_FILE, USER_DATA)

        const CLICK_COOKIE_NUMBER = USER_DATA['SI']['点击曲奇次数']
        return e.reply((e.adapter === 'QQBot') ? [
            `# 第${CLICK_COOKIE_NUMBER}次点击曲奇`,
            `> [曲奇]+${INITIAL_QUANTITY}`,
            Bot.Button([[
                { label: '+1', callback: '/点击曲奇' }
            ], [
                { label: '领取收益', enter: true },
                { label: '曲奇菜单', enter: true }
            ]])
        ] : [
            segment.at(USER_ID),
            `\n第${CLICK_COOKIE_NUMBER}次点击曲奇`,
            `\n[曲奇]+${INITIAL_QUANTITY}`
        ])
    }
}

function CREATING_USER_INFORMATION(ID) {
    const USER_FILE = `${ALL_USER_FOLDER}${ID}.json`

    SAVE_DATA(USER_FILE, {
        ID: ID,
        SI: {
            开始时间: getCurrentDate(),
            当前饼干数量: 0,
            当前总饼干生产数量: 0,
            总生产饼干数量: 0,
            飞升失去饼干量: 0,
            飞升次数: 0,
            拥有建筑: 60,
            每秒饼干产量: 0,
            点击曲奇产量: 0,
            点击曲奇次数: 0,
            点击曲奇总产量: 0
        },
        BI: {
            指针: 1, 指针每秒收益: 0.1, 指针时间戳: Date.now(),
            老太婆: 0, 老太婆每秒收益: 0, 老太婆时间戳: 0,
            农场: 0, 农场每秒收益: 0, 农场时间戳: 0,
            矿坑: 0, 矿坑每秒收益: 0, 矿坑时间戳: 0,
            工厂: 0, 工厂每秒收益: 0, 工厂时间戳: 0,
            银行: 0, 银行每秒收益: 0, 银行时间戳: 0,
            神庙: 0, 神庙每秒收益: 0, 神庙时间戳: 0,
            巫师塔: 0, 巫师塔每秒收益: 0, 巫师塔时间戳: 0,
            装运飞船: 0, 装运飞船每秒收益: 0, 装运飞船时间戳: 0,
            炼金术实验室: 0, 炼金术实验室每秒收益: 0, 炼金术实验室时间戳: 0,
            传送门: 0, 传送门每秒收益: 0, 传送门时间戳: 0,
            时光机: 0, 时光机每秒收益: 0, 时光机时间戳: 0,
            反物质聚光镜: 0, 反物质聚光镜每秒收益: 0, 反物质聚光镜时间戳: 0,
            棱镜: 0, 棱镜每秒收益: 0, 棱镜时间戳: 0,
            机会制造器: 0, 机会制造器每秒收益: 0, 机会制造器时间戳: 0,
            分形引擎: 0, 分形引擎每秒收益: 0, 分形引擎时间戳: 0,
            Javascript控制台: 0, Javascript控制台每秒收益: 0, Javascript控制台时间戳: 0,
            空闲宇宙: 0, 空闲宇宙每秒收益: 0, 空闲宇宙时间戳: 0,
            脑皮屑面包师: 0, 脑皮屑面包师每秒收益: 0, 脑皮屑面包师时间戳: 0,
            你: 0, 你每秒收益: 0, 你时间戳: 0
        },
        UI: {}
    })
}

function getCurrentDate() {
    const date = new Date();
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, '0');
    const day = String(date.getDate()).padStart(2, '0');
    return `${year}-${month}-${day}`;
}
