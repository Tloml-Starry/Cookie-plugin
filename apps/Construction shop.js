import { render } from '../components/index.js';

const BUILDING_UPGRADE_SHOP_REGEX = /^(#|\/)?(.*)升级商店$/
export class COOKIE_PLUGIN extends plugin {
    constructor() {
        super({
            name: '[Cookie]建筑商店',
            dsc: '建筑商店',
            event: 'message',
            priority: 1,
            rule: [{
                reg: /^(#|\/)?建筑商店$/,
                fnc: 'BUILDING_SHOP'
            }, {
                reg: BUILDING_UPGRADE_SHOP_REGEX,
                fnc: 'BUILDING_UPGRADE_SHOP'
            }]
        })
    }

    async BUILDING_SHOP(e) { return render('html/BUILDING SHOP', { null: null }, { e, scale: 1.4 }) }
    async BUILDING_UPGRADE_SHOP(e) {
        const BUILDING = e.msg.match(BUILDING_UPGRADE_SHOP_REGEX)[2]
        if (BUILDING === '指针') return render('html/UPGRADES CURSOR', { null: null, }, { e, scale: 1.4 })
        else if (BUILDING === '老太婆') return render('html/UPGRADES GRANDMA', { null: null, }, { e, scale: 1.4 })
        return e.reply('暂无此建筑升级商店')
    }
}