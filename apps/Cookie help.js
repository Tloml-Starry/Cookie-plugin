import { render, Data } from '../components/index.js';
import lodash from 'lodash';

export class COOKIE_PLUGIN extends plugin {
    constructor() {
        super({
            name: '[Cookie]曲奇菜单',
            event: 'message',
            priority: 1,
            rule: [{
                reg: /^(#|\/)?(曲奇|Cookie)菜单$/,
                fnc: 'COOKIE_HELP',
            }]
        })
    }

    async COOKIE_HELP() { return HELP(this.e) }
}

async function HELP(e) {
    let custom = {};
    let help = {};

    let { diyCfg, sysCfg } = await Data.importCfg('help');
    custom = help;

    let helpConfig = lodash.defaults(diyCfg.helpCfg || {}, custom.helpCfg, sysCfg.helpCfg);

    let helpList = diyCfg.helpList || custom.helpList || sysCfg.helpList;

    let helpGroup = [];

    for (let group of helpList) {
        if (group.auth && group.auth === 'master' && !e.isMaster) {
            continue;
        }
        for (let help of group.list) {
            let icon = help.icon * 1;

            if (!icon) {
                help.css = 'display:none';
            } else {
                let x = (icon - 1) % 10;
                let y = (icon - x - 1) / 10;
                help.css = `background-position:-${x * 50}px -${y * 50}px`;
            }
        }
        helpGroup.push(group);
    }

    let colCount = 3;

    return await render('html/HELP', {
        helpCfg: helpConfig,
        helpGroup,
        colCount,
        element: 'default',
    }, {
        e,
        scale: 2.0,
    });
}