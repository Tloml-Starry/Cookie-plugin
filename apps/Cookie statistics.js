import { render } from '../components/index.js';
import { Convert } from '../utils/Nc.js';
import { GET_DATA, IS_THERE_USER } from '../utils/Gs.js';

export class COOKIE_PLUGIN extends plugin {
    constructor() {
        super({
            name: '[Cookie]曲奇统计信息',
            dsc: '个人曲奇统计信息',
            exent: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?曲奇统计信息$/,
                    fnc: 'C_STATISTICS'
                }
            ]
        })
    }

    async C_STATISTICS(e) {
        const USER_ID = e.user_id

        if (!IS_THERE_USER(USER_ID)) return e.reply((e.adapter === 'QQBot') ? [
            '> 您尚未游玩过曲奇点点乐',
            Bot.Button([[{ label: '游玩', data: '/点击曲奇' }]])
        ] : [
            segment.at(USER_ID),
            '\n您尚未游玩过曲奇点点乐',
            '\n请先发送[点击曲奇]'
        ])

        const USER_FILE = `plugins/Cookie-plugin/data/USER INFO/${USER_ID}.json`
        const USER_DATA = GET_DATA(USER_FILE)

        const ID = USER_DATA['ID'].replace(/102076896-/g, '')
        const START_TIME = USER_DATA['SI']['开始时间']
        const CBQ = Convert(USER_DATA['SI']['当前饼干数量'])
        const CTBP = Convert(USER_DATA['SI']['当前总饼干生产数量'])
        const TNOBP = Convert(USER_DATA['SI']['总生产饼干数量'])
        const FUALCA = Convert(USER_DATA['SI']['飞升失去饼干量'])
        const NOA = USER_DATA['SI']['飞升次数']
        const OAB = USER_DATA['SI']['拥有建筑']
        const BPPS = Convert(USER_DATA['SI']['每秒饼干产量'])
        const CCO = Convert(USER_DATA['SI']['点击曲奇产量'])
        const NOCC = Convert(USER_DATA['SI']['点击曲奇次数'])
        const TCCO = Convert(USER_DATA['SI']['点击曲奇总产量'])

        if (e.adapter === 'QQBot') return e.reply([
            '# 曲奇统计信息',
            `> ${ID}`,
            `开始时间：${START_TIME}`,
            `当前饼干数量：${CBQ}`,
            `当前饼干总生产量：${CTBP}`,
            `总生产饼干量：${TNOBP}`,
            `飞升失去饼干量：${FUALCA}`,
            `飞升次数：${NOA}`,
            `拥有建筑：${OAB}`,
            `每秒饼干生产量：${BPPS}`,
            `点击曲奇产量：${CCO}`,
            `点击曲奇次数：${NOCC}`,
            `点击曲奇总产量：${TCCO}`
        ])

        await render('html/Cookie statistics', {
            ID, Start_time: START_TIME,
            Current_biscuit_quantity: CBQ,
            Current_total_biscuit_production: CTBP,
            Total_number_of_biscuits_produced: TNOBP,
            Fly_up_and_lose_cookie_amount: FUALCA,
            Number_of_ascent: NOA,
            Own_a_building: OAB,
            Biscuit_production_per_second: BPPS,
            Click_cookie_output: CCO,
            Number_of_cookie_clicks: NOCC,
            Total_click_cookie_output: TCCO
        }, { e, scale: 1.4 })
    }
}