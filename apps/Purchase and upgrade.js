import { GET_DATA, SAVE_DATA, IS_THERE_USER } from '../utils/Gs.js';
import { Convert } from '../utils/Nc.js';

const ALL_BUILDING = ['指针', '老太婆', '农场', '矿坑', '工厂', '银行', '神庙', '巫师塔', '装运飞船', '炼金术实验室', '传送门', '时光机', '反物质聚光镜', '棱镜', '机会制造器', '分形引擎', 'Javascript控制台', '空闲宇宙', '脑皮屑面包师', '你']
const ALL_UPGRADE_ITEM = [
    [
        '强化食指', '腕管护理霜', '左右开弓', '千手指', '百万手指',
        '十亿手指', '一兆手指', '千兆手指', '一京手指', '一垓手指',
        '一秭手指', '千秭手指', '100穰手指', '十进制手指', '手指',
    ]
]
const UPGRADE_1 = [
    '强化食指'
]
const UPGRADE_PRICE_LIST = {
    "强化食指": 1e2, "腕管护理霜": 5e2, "左右开弓": 1e4, "千手指": 1e5, "百万手指": 1e7, "十亿手指": 1e8, "一兆手指": 1e9, "千兆手指": 1e10, "一京手指": 1e13, "一垓手指": 1e16, "一秭手指": 1e19, "千秭手指": 1e22, "100穰手指": 1e25, "十进制手指": 1e28, "手指": 1e31
}

const REGEX = /^(#|\/)?购置(.*)升级(.*)$/
export class COOKIE_PLUGIN extends plugin {
    constructor() {
        super({
            name: '[Cookie]购置升级',
            dsc: '购买升级',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: REGEX,
                    fnc: 'Purchase_and_upgrade'
                }
            ]
        })
    }

    async Purchase_and_upgrade(e) {
        const USER_ID = e.user_id

        if (!IS_THERE_USER(USER_ID)) return e.reply((e.adapter === 'QQBot') ? [
            '> 您尚未游玩过曲奇点点乐',
            Bot.Button([[{ label: '游玩', data: '/点击曲奇' }]])
        ] : [
            segment.at(USER_ID),
            '\n您尚未游玩过曲奇点点乐',
            '\n请先发送[点击曲奇]'
        ])

        const Match = e.msg.match(REGEX)
        const BUILDING = Match[2]
        const UPGRADE_ITEM = Match[3]

        const ALL_UPGRADE = ALL_UPGRADE_ITEM.flat()

        if (!ALL_BUILDING.includes(BUILDING)) return e.reply(['> 无此建筑'])
        if (!ALL_UPGRADE.includes(UPGRADE_ITEM)) return e.reply(['> 无此升级项'])

        const USER_FILE = `plugins/Cookie-plugin/data/USER INFO/${USER_ID}.json`
        const USER_DATA = GET_DATA(USER_FILE)

        const ARRAY_UPGRADE_ITEM = ALL_UPGRADE_ITEM[ALL_BUILDING.indexOf(BUILDING)]

        if (USER_DATA['UI'][UPGRADE_ITEM]) {
            const REPLY = (e.adapter === 'QQBot') ? ['# 购买失败！', '> 您已拥有该升级'] : '购买失败，您已拥有该升级'
            return e.reply(REPLY)
        }
        let i = (ARRAY_UPGRADE_ITEM.indexOf(UPGRADE_ITEM)) - 1
        if (!UPGRADE_1.includes(UPGRADE_ITEM) && !USER_DATA['UI'][ARRAY_UPGRADE_ITEM[i]]) {
            const REPLY = (e.adapter === 'QQBot') ? ['# 购买失败！', `> 您还没购买**${ARRAY_UPGRADE_ITEM[i]}**！无法购买后面的升级项`] : `购买失败！\n您还没购买${ARRAY_UPGRADE_ITEM[i]}!无法购买后面的升级项`
            return e.reply(REPLY)
        }

        const CURRENT_NUMBER = USER_DATA['SI']['当前饼干数量']
        const UPGRADE_PRICE = UPGRADE_PRICE_LIST[UPGRADE_ITEM]

        if (CURRENT_NUMBER < UPGRADE_PRICE) {
            const REPLY = (e.adapter === 'QQBot') ? ['# 购买失败！', '> 曲奇数量不足', `缺少曲奇${Convert(UPGRADE_PRICE - CURRENT_NUMBER)}`] : `购买失败!\n曲奇数量不足\n缺少曲奇${Convert(UPGRADE_PRICE - CURRENT_NUMBER)}`
            return e.reply(REPLY)
        }

        USER_DATA['SI']['当前饼干数量'] -= UPGRADE_PRICE
        USER_DATA['UI'][UPGRADE_ITEM] = true

        SAVE_DATA(USER_FILE, USER_DATA)

        const REPLY = (e.adapter === 'QQBot') ? ['# 购买成功！', `> 消耗曲奇${Convert(UPGRADE_PRICE)}`] : `购买成功！\n消耗曲奇${Convert(UPGRADE_PRICE)}`
        return e.reply(REPLY)
    }
}