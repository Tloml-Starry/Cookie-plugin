import { cop } from '../utils/Calculation of price.js';
import { GET_DATA, IS_THERE_USER } from '../utils/Gs.js';

const BUILDINGS = {
    '指针': 0.1,
    '老太婆': 1,
    '农场': 8,
    '矿坑': 47,
    '工厂': 260,
    '银行': 14e2,
    '神庙': 78e2,
    '巫师塔': 44e3,
    '装运飞船': 26e4,
    '炼金术实验室': 16e5,
    '传送门': 1e7,
    '时光机': 65e6,
    '反物质聚光镜': 43e7,
    '棱镜': 29e8,
    '机会制造器': 21e9,
    '分形引擎': 15e10,
    'Javascript控制台': 11e11,
    '空闲宇宙': 83e11,
    '脑皮屑面包师': 64e12,
    '你': 51e13
};

export class COOKIE_PLUGIN extends plugin {
    constructor() {
        super({
            name: '[Cookie]购置建筑',
            dsc: '购买建筑',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?购置建筑(.*)\*(\d+)$/,
                    fnc: 'Purchase_of_building'
                }
            ]
        })
    }

    async Purchase_of_building(e) {
        const USER_ID = e.user_id

        if (!IS_THERE_USER(USER_ID)) return e.reply((e.adapter === 'QQBot') ? [
            '> 您尚未游玩过曲奇点点乐',
            Bot.Button([[{ label: '游玩', data: '/点击曲奇' }]])
        ] : [
            segment.at(USER_ID),
            '\n您尚未游玩过曲奇点点乐',
            '\n请先发送[点击曲奇]'
        ])

        const [, , building, quantity] = e.msg.match(/^(#|\/)?购置建筑(.*)\*(\d+)$/);

        if (!Number.isInteger(Number(quantity))) {
            return e.reply('请输入整数！');
        } else if (Number(quantity) <= 0) {
            return e.reply('请输入0以上的单位！');
        }

        const price = BUILDINGS[building];

        if (!price) return e.reply('无此建筑，请发送建筑商店查看所有建筑');

        const Build = 'plugins/Cookie-plugin/data/Game_Store/Build.json';
        const BuildJson = GET_DATA(Build);

        cop(e, BuildJson[building], building, Number(quantity), price);
    }
}
