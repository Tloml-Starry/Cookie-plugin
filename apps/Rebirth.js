import { GET_DATA, SAVE_DATA, IS_THERE_USER } from '../utils/Gs.js';
import { Convert } from '../utils/Nc.js';

const ALL_USER_FOLDER = 'plugins/Cookie-plugin/data/USER INFO/'
export class COOKIE_PLUGIN extends plugin {
    constructor() {
        super({
            name: '[Cookie]飞升',
            dsc: '血肉苦难 机械飞升！(doge)',
            event: 'message',
            priority: 1,
            rule: [{
                reg: /^(#|\/)?飞升$/,
                fnc: 'REBIRTH'
            }]
        })
    }

    async REBIRTH(e) {
        const USER_ID = e.user_id
        const USER_FILE = `${ALL_USER_FOLDER}${USER_ID}.json`

        if (!IS_THERE_USER(USER_ID)) return e.reply((e.adapter === 'QQBot') ? [
            '> 您尚未游玩过曲奇点点乐',
            Bot.Button([[{ label: '游玩', data: '/点击曲奇' }]])
        ] : [
            segment.at(USER_ID),
            '\n您尚未游玩过曲奇点点乐',
            '\n请先发送[点击曲奇]'
        ])

        const USER_DATA = GET_DATA(USER_FILE)
        const USER_COOKIE_N = USER_DATA['SI']['当前饼干数量']

        if (USER_COOKIE_N <= 999999999999999) return e.reply((e.adapter === 'QQBot') ? [
            '> ', segment.at(USER_ID),
            '当前飞升无法得到任何天堂碎片和威望'
        ] : [
            segment.at(USER_ID),
            '\n当前飞升无法得到任何天堂碎片和威望'
        ])

        const HEAVENLY_CHIPS = Math.floor(Math.ceil(parseFloat(USER_COOKIE_N / 1000000000000000)))
        REBIRTH_RESET(USER_ID, HEAVENLY_CHIPS)

        return e.reply((e.adapter === 'QQBot') ? [
            '# 飞升成功',
            `> 你获得了**${Convert(HEAVENLY_CHIPS)}**份天堂碎片`,
            '下次飞升此碎片将重置，请您记得在天堂商店使用',
            `当前威望等级${Convert(USER_DATA['SI']['威望等级'])}`
        ] : [
            '\n飞升成功',
            `\n你获得了${Convert(HEAVENLY_CHIPS)}份天堂碎片`,
            '\n下次飞升此碎片将重置，请您记得在天堂商店使用',
            `\n当前威望等级${Convert(USER_DATA['SI']['威望等级'])}`
        ])
    }
}

function REBIRTH_RESET(ID, NUMBER) {
    const USER_FILE = `${ALL_USER_FOLDER}${ID}.json`
    const USER_DATA = GET_DATA(USER_FILE)

    SAVE_DATA(USER_FILE, {
        ID: ID,
        SI: {
            开始时间: USER_DATA['SI']['开始时间'],
            当前饼干数量: 0,
            当前总饼干生产数量: 0,
            总生产饼干数量: USER_DATA['SI']['总生产饼干数量'],
            飞升失去饼干量: USER_DATA['SI']['飞升失去饼干量'] + NUMBER,
            飞升次数: USER_DATA['SI']['飞升次数'] + 1,
            拥有建筑: 0,
            每秒饼干产量: 0,
            点击曲奇产量: 0,
            点击曲奇次数: USER_DATA['SI']['点击曲奇次数'],
            点击曲奇总产量: USER_DATA['SI']['点击曲奇总产量'],
            威望等级: (USER_DATA['SI']['威望等级'] || 0) + NUMBER,
            天堂碎片: NUMBER
        },
        BI: {
            指针: 1, 指针每秒收益: 0.1, 指针时间戳: Date.now(),
            老太婆: 0, 老太婆每秒收益: 0, 老太婆时间戳: 0,
            农场: 0, 农场每秒收益: 0, 农场时间戳: 0,
            矿坑: 0, 矿坑每秒收益: 0, 矿坑时间戳: 0,
            工厂: 0, 工厂每秒收益: 0, 工厂时间戳: 0,
            银行: 0, 银行每秒收益: 0, 银行时间戳: 0,
            神庙: 0, 神庙每秒收益: 0, 神庙时间戳: 0,
            巫师塔: 0, 巫师塔每秒收益: 0, 巫师塔时间戳: 0,
            装运飞船: 0, 装运飞船每秒收益: 0, 装运飞船时间戳: 0,
            炼金术实验室: 0, 炼金术实验室每秒收益: 0, 炼金术实验室时间戳: 0,
            传送门: 0, 传送门每秒收益: 0, 传送门时间戳: 0,
            时光机: 0, 时光机每秒收益: 0, 时光机时间戳: 0,
            反物质聚光镜: 0, 反物质聚光镜每秒收益: 0, 反物质聚光镜时间戳: 0,
            棱镜: 0, 棱镜每秒收益: 0, 棱镜时间戳: 0,
            机会制造器: 0, 机会制造器每秒收益: 0, 机会制造器时间戳: 0,
            分形引擎: 0, 分形引擎每秒收益: 0, 分形引擎时间戳: 0,
            Javascript控制台: 0, Javascript控制台每秒收益: 0, Javascript控制台时间戳: 0,
            空闲宇宙: 0, 空闲宇宙每秒收益: 0, 空闲宇宙时间戳: 0,
            脑皮屑面包师: 0, 脑皮屑面包师每秒收益: 0, 脑皮屑面包师时间戳: 0,
            你: 0, 你每秒收益: 0, 你时间戳: 0
        },
        UI: {}
    })
}