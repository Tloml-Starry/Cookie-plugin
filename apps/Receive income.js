import fs from 'fs';
import lodash from 'lodash';
import { render } from '../components/index.js';
import { GET_DATA, SAVE_DATA, IS_THERE_USER } from '../utils/Gs.js';
import { Convert } from '../utils/Nc.js';

const ALL_BUILDING = ['指针', '老太婆', '农场', '矿坑', '工厂', '银行', '神庙', '巫师塔', '装运飞船', '炼金术实验室', '传送门', '时光机', '反物质聚光镜', '棱镜', '机会制造器', '分形引擎', 'Javascript控制台', '空闲宇宙', '脑皮屑面包师', '你']

export class COOKIE_PLUGIN extends plugin {
    constructor() {
        super({
            name: '[Cookie]领取收益',
            dsc: '领取建筑收益',
            event: 'message',
            priority: 1,
            rule: [
                {
                    reg: /^(#|\/)?领取收益$/,
                    fnc: 'Receive_income'
                }
            ]
        })
    }

    async Receive_income(e) {
        const USER_ID = e.user_id

        if (!IS_THERE_USER(USER_ID)) return e.reply((e.adapter === 'QQBot') ? [
            '> 您尚未游玩过曲奇点点乐',
            Bot.Button([[{ label: '游玩', data: '/点击曲奇' }]])
        ] : [
            segment.at(USER_ID),
            '\n您尚未游玩过曲奇点点乐',
            '\n请先发送[点击曲奇]'
        ])

        const CURRENT_TIME_STAMP = Date.now();

        const USER_FILE = `plugins/Cookie-plugin/data/USER INFO/${USER_ID}.json`
        const USER_DATA = GET_DATA(USER_FILE)

        for (let i = 0; i < ALL_BUILDING.length; i++) {
            if (USER_DATA['BI'][ALL_BUILDING[i]] === 0) {
                USER_DATA['BI'][`${ALL_BUILDING[i]}时间戳`] = CURRENT_TIME_STAMP;
            }
        }

        // 建筑收益 - 无升级
        let B1_E = I_C(USER_DATA['BI']['指针时间戳'], USER_DATA['BI']['指针每秒收益'])
        const B2_E = I_C(USER_DATA['BI']['老太婆时间戳'], USER_DATA['BI']['老太婆每秒收益'])
        const B3_E = I_C(USER_DATA['BI']['农场时间戳'], USER_DATA['BI']['农场每秒收益'])
        const B4_E = I_C(USER_DATA['BI']['矿坑时间戳'], USER_DATA['BI']['矿坑每秒收益'])
        const B5_E = I_C(USER_DATA['BI']['工厂时间戳'], USER_DATA['BI']['工厂每秒收益'])
        const B6_E = I_C(USER_DATA['BI']['银行时间戳'], USER_DATA['BI']['银行每秒收益'])
        const B7_E = I_C(USER_DATA['BI']['神庙时间戳'], USER_DATA['BI']['神庙每秒收益'])
        const B8_E = I_C(USER_DATA['BI']['巫师塔时间戳'], USER_DATA['BI']['巫师塔每秒收益'])
        const B9_E = I_C(USER_DATA['BI']['装运飞船时间戳'], USER_DATA['BI']['装运飞船每秒收益'])
        const B10_E = I_C(USER_DATA['BI']['炼金术实验室时间戳'], USER_DATA['BI']['炼金术实验室每秒收益'])
        const B11_E = I_C(USER_DATA['BI']['传送门时间戳'], USER_DATA['BI']['传送门每秒收益'])
        const B12_E = I_C(USER_DATA['BI']['时光机时间戳'], USER_DATA['BI']['时光机每秒收益'])
        const B13_E = I_C(USER_DATA['BI']['反物质聚光镜时间戳'], USER_DATA['BI']['反物质聚光镜每秒收益'])
        const B14_E = I_C(USER_DATA['BI']['棱镜时间戳'], USER_DATA['BI']['棱镜每秒收益'])
        const B15_E = I_C(USER_DATA['BI']['机会制造器时间戳'], USER_DATA['BI']['机会制造器每秒收益'])
        const B16_E = I_C(USER_DATA['BI']['分形引擎时间戳'], USER_DATA['BI']['分形引擎每秒收益'])
        const B17_E = I_C(USER_DATA['BI']['Javascript控制台时间戳'], USER_DATA['BI']['Javascript控制台每秒收益'])
        const B18_E = I_C(USER_DATA['BI']['空闲宇宙时间戳'], USER_DATA['BI']['空闲宇宙每秒收益'])
        const B19_E = I_C(USER_DATA['BI']['脑皮屑面包师时间戳'], USER_DATA['BI']['脑皮屑面包师每秒收益'])
        const B20_E = I_C(USER_DATA['BI']['你时间戳'], USER_DATA['BI']['你每秒收益'])

        // 建筑数量
        const B1_N = USER_DATA['BI']['指针']
        const B2_N = USER_DATA['BI']['老太婆']
        const B3_N = USER_DATA['BI']['农场']
        const B4_N = USER_DATA['BI']['矿坑']
        const B5_N = USER_DATA['BI']['工厂']
        const B6_N = USER_DATA['BI']['银行']
        const B7_N = USER_DATA['BI']['神庙']
        const B8_N = USER_DATA['BI']['巫师塔']
        const B9_N = USER_DATA['BI']['装运飞船']
        const B10_N = USER_DATA['BI']['炼金术实验室']
        const B11_N = USER_DATA['BI']['传送门']
        const B12_N = USER_DATA['BI']['时光机']
        const B13_N = USER_DATA['BI']['反物质聚光镜']
        const B14_N = USER_DATA['BI']['棱镜']
        const B15_N = USER_DATA['BI']['机会制造器']
        const B16_N = USER_DATA['BI']['分形引擎']
        const B17_N = USER_DATA['BI']['Javascript控制台']
        const B18_N = USER_DATA['BI']['空闲宇宙']
        const B19_N = USER_DATA['BI']['脑皮屑面包师']
        const B20_N = USER_DATA['BI']['你']

        const TR = B1_E + B2_E + B3_E + B4_E + B5_E + B6_E + B7_E + B8_E + B9_E + B10_E + B11_E + B12_E + B13_E + B14_E + B15_E + B16_E + B17_E + B18_E + B19_E + B20_E
        const TQ = B1_N + B2_N + B3_N + B4_N + B5_N + B6_N + B7_N + B8_N + B9_N + B10_N + B11_N + B12_N + B13_N + B14_N + B15_N + B16_N + B17_N + B18_N + B19_N + B20_N

        // 建筑收益 - 有升级
        if (USER_DATA['UI']['强化食指']) B1_E *= 2
        if (USER_DATA['UI']['腕管护理霜']) B1_E *= 2
        if (USER_DATA['UI']['左右开弓']) B1_E *= 2
        let TFE = 0
        if (USER_DATA['UI']['千手指']) TFE = (TQ - B1_N) * 0.1
        if (USER_DATA['UI']['百万手指']) TFE *= 5
        if (USER_DATA['UI']['十亿手指']) TFE *= 10
        if (USER_DATA['UI']['一兆手指']) TFE *= 20
        if (USER_DATA['UI']['千兆手指']) TFE *= 20
        if (USER_DATA['UI']['一京手指']) TFE *= 20
        if (USER_DATA['UI']['一垓手指']) TFE *= 20
        if (USER_DATA['UI']['一秭手指']) TFE *= 20
        if (USER_DATA['UI']['千秭手指']) TFE *= 20
        if (USER_DATA['UI']['100穰手指']) TFE *= 20
        if (USER_DATA['UI']['十进制手指']) TFE *= 20
        if (USER_DATA['UI']['手指']) TFE *= 20
        B1_E += TFE

        const PT = {};

        for (const B of ALL_BUILDING) { PT[B] = PTS(USER_DATA['BI'][`${B}时间戳`]) }


        for (let i = 0; i < ALL_BUILDING.length; i++) { USER_DATA['BI'][`${ALL_BUILDING[i]}时间戳`] = CURRENT_TIME_STAMP }

        const stats = ['当前饼干数量', '当前总饼干生产数量', '总生产饼干数量']

        for (let i = 0; i < stats.length; i++) { USER_DATA['SI'][stats[i]] += TR }

        SAVE_DATA(USER_FILE, USER_DATA)

        const html = {
            TBN: TQ,
            TR: Convert(TR),
            CQ: Convert(USER_DATA['SI']['当前饼干数量']),
            B1_N, B2_N, B3_N, B4_N, B5_N,
            B6_N, B7_N, B8_N, B9_N, B10_N,
            B11_N, B12_N, B13_N, B14_N, B15_N,
            B16_N, B17_N, B18_N, B19_N, B20_N,
            B1_E: Convert(B1_E), B2_E: Convert(B2_E), B3_E: Convert(B3_E), B4_E: Convert(B4_E),
            B5_E: Convert(B5_E), B6_E: Convert(B6_E), B7_E: Convert(B7_E), B8_E: Convert(B8_E),
            B9_E: Convert(B9_E), B10_E: Convert(B10_E), B11_E: Convert(B11_E), B12_E: Convert(B12_E),
            B13_E: Convert(B13_E), B14_E: Convert(B14_E), B15_E: Convert(B15_E), B16_E: Convert(B16_E),
            B17_E: Convert(B17_E), B18_E: Convert(B18_E), B19_E: Convert(B19_E), B20_E: Convert(B20_E),
            Pointer_production_time: PT.指针,
            Productive_time_of_old_woman: PT.老太婆,
            Farm_production_time: PT.农场,
            Pit_production_time: PT.矿坑,
            Factory_production_time: PT.工厂,
            Bank_production_time: PT.银行,
            Temple_production_time: PT.神庙,
            Wizard_Tower_production_time: PT.巫师塔,
            Shipping_spacecraft_production_time: PT.装运飞船,
            Alchemy_lab_production_time: PT.炼金术实验室,
            Portal_production_time: PT.传送门,
            Time_machine_production_time: PT.时光机,
            Antimatter_condenser_production_time: PT.反物质聚光镜,
            Prism_production_time: PT.棱镜,
            Opportunity_maker_production_time: PT.机会制造器,
            Fractal_engine_production_time: PT.分形引擎,
            Javascript_console_production_time: PT.Javascript控制台,
            Idle_universe_production_time: PT.空闲宇宙,
            Brain_zest_baker_production_time: PT.脑皮屑面包师,
            Your_production_time: PT.你
        }
        let bg = await rodom();
        await render('html/Take full benefits', { ...html, bg }, { e, scale: 1.4 });
    }
}

function I_C(TS, RPS) {
    return parseFloat(((Math.floor((Date.now() - TS) / 1000)) * RPS).toFixed(0))
}

function PTS(TS) {
    const CURRENT_TIME_STAMP = Date.now();
    const B_TS = new Date(CURRENT_TIME_STAMP - TS);
    const day = Math.floor(B_TS / (24 * 60 * 60 * 1000))
    const Hour = Math.floor((B_TS % (24 * 60 * 60 * 1000)) / (60 * 60 * 1000));
    const minutes = Math.floor((B_TS % (60 * 60 * 1000)) / (60 * 1000));
    const second = Math.floor((B_TS % (60 * 1000)) / 1000);

    let Production_Time = '';

    if (day !== 0) { Production_Time = `${day}天${Hour}时${minutes}分${second}秒` }
    else if (Hour !== 0) { Production_Time = `${Hour}时${minutes}分${second}秒` }
    else if (minutes !== 0) { Production_Time = `${minutes}分${second}秒` }
    else if (second !== 0) { Production_Time = `${second}秒` }
    else { Production_Time = '无' }
    return Production_Time
}

async function rodom() {
    let image = fs.readdirSync('./plugins/Cookie-plugin/resources/img/backdrop/');

    let list_img = Array.from(image);

    let theme = list_img.length == 1 ? list_img[0] : list_img[lodash.random(0, list_img.length - 1)];

    return theme;
}