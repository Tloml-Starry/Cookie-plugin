import fs from 'fs';

export class COOKIE_PLUGIN extends plugin {
    constructor() {
        super({
            name: '[Cookie]重置用户存档信息',
            dsc: 'Cookie-Plugin',
            event: 'message',
            priority: 1,
            rule: [{
                reg: /^.*$/,
                log: false,
                fnc: 'reset'
            }]
        })
    }

    async reset() {
        const ALL_USER_FOLDER = 'plugins/Cookie-plugin/data/Userinfo/'

        if (!fs.existsSync(ALL_USER_FOLDER)) return false
        const USER_FILE_DIRECTORY = fs.readdirSync(ALL_USER_FOLDER)

        USER_FILE_DIRECTORY.forEach((USER_FILE_NAME) => {
            const USER_FILE = `${ALL_USER_FOLDER}${USER_FILE_NAME}`
            const USER_SJC = `${USER_FILE}/${USER_FILE_NAME}.json`
            const USER_JZ = `${USER_FILE}/User_Building/${USER_FILE_NAME}.json`
            const USER_TJ = `${USER_FILE}/User_Statistics/${USER_FILE_NAME}.json`

            const SJC_DATA = JSON.parse(fs.readFileSync(USER_SJC, 'utf8'))
            const JZ_DATA = JSON.parse(fs.readFileSync(USER_JZ, 'utf8'))
            const TJ_DATA = JSON.parse(fs.readFileSync(USER_TJ, 'utf8'))

            const USER_DATA = {
                ID: TJ_DATA[USER_FILE_NAME]['ID'],
                SI: {
                    开始时间: TJ_DATA[USER_FILE_NAME]['开始时间'],
                    当前饼干数量: TJ_DATA[USER_FILE_NAME]['当前饼干数量'],
                    当前总饼干生产数量: TJ_DATA[USER_FILE_NAME]['当前总饼干生产数量'],
                    总生产饼干数量: TJ_DATA[USER_FILE_NAME]['总生产饼干数量'],
                    飞升失去饼干量: TJ_DATA[USER_FILE_NAME]['飞升失去饼干量'],
                    飞升次数: TJ_DATA[USER_FILE_NAME]['飞升次数'],
                    拥有建筑: TJ_DATA[USER_FILE_NAME]['拥有建筑'],
                    每秒饼干产量: TJ_DATA[USER_FILE_NAME]['每秒饼干产量'],
                    点击曲奇产量: TJ_DATA[USER_FILE_NAME]['点击曲奇产量'],
                    点击曲奇次数: TJ_DATA[USER_FILE_NAME]['点击曲奇次数'],
                    点击曲奇总产量: TJ_DATA[USER_FILE_NAME]['点击曲奇总产量']
                },
                BI: {
                    指针: JZ_DATA[USER_FILE_NAME]['指针'], 指针每秒收益: JZ_DATA[USER_FILE_NAME]['指针每秒收益'], 指针时间戳: SJC_DATA[USER_FILE_NAME]['指针时间戳'],
                    老太婆: JZ_DATA[USER_FILE_NAME]['老太婆'], 老太婆每秒收益: JZ_DATA[USER_FILE_NAME]['老太婆每秒收益'], 老太婆时间戳: SJC_DATA[USER_FILE_NAME]['老太婆时间戳'],
                    农场: JZ_DATA[USER_FILE_NAME]['农场'], 农场每秒收益: JZ_DATA[USER_FILE_NAME]['农场每秒收益'], 农场时间戳: SJC_DATA[USER_FILE_NAME]['农场时间戳'],
                    矿坑: JZ_DATA[USER_FILE_NAME]['矿坑'], 矿坑每秒收益: JZ_DATA[USER_FILE_NAME]['矿坑每秒收益'], 矿坑时间戳: SJC_DATA[USER_FILE_NAME]['矿坑时间戳'],
                    工厂: JZ_DATA[USER_FILE_NAME]['工厂'], 工厂每秒收益: JZ_DATA[USER_FILE_NAME]['工厂每秒收益'], 工厂时间戳: SJC_DATA[USER_FILE_NAME]['工厂时间戳'],
                    银行: JZ_DATA[USER_FILE_NAME]['银行'], 银行每秒收益: JZ_DATA[USER_FILE_NAME]['银行每秒收益'], 银行时间戳: SJC_DATA[USER_FILE_NAME]['银行时间戳'],
                    神庙: JZ_DATA[USER_FILE_NAME]['神庙'], 神庙每秒收益: JZ_DATA[USER_FILE_NAME]['神庙每秒收益'], 神庙时间戳: SJC_DATA[USER_FILE_NAME]['神庙时间戳'],
                    巫师塔: JZ_DATA[USER_FILE_NAME]['巫师塔'], 巫师塔每秒收益: JZ_DATA[USER_FILE_NAME]['巫师塔每秒收益'], 巫师塔时间戳: SJC_DATA[USER_FILE_NAME]['巫师塔时间戳'],
                    装运飞船: JZ_DATA[USER_FILE_NAME]['装运飞船'], 装运飞船每秒收益: JZ_DATA[USER_FILE_NAME]['装运飞船每秒收益'], 装运飞船时间戳: SJC_DATA[USER_FILE_NAME]['装运飞船时间戳'],
                    炼金术实验室: JZ_DATA[USER_FILE_NAME]['炼金术实验室'], 炼金术实验室每秒收益: JZ_DATA[USER_FILE_NAME]['炼金术实验室每秒收益'], 炼金术实验室时间戳: SJC_DATA[USER_FILE_NAME]['炼金术实验室时间戳'],
                    传送门: JZ_DATA[USER_FILE_NAME]['传送门'], 传送门每秒收益: JZ_DATA[USER_FILE_NAME]['传送门每秒收益'], 传送门时间戳: SJC_DATA[USER_FILE_NAME]['传送门时间戳'],
                    时光机: JZ_DATA[USER_FILE_NAME]['时光机'], 时光机每秒收益: JZ_DATA[USER_FILE_NAME]['时光机每秒收益'], 时光机时间戳: SJC_DATA[USER_FILE_NAME]['时光机时间戳'],
                    反物质聚光镜: JZ_DATA[USER_FILE_NAME]['反物质聚光镜'], 反物质聚光镜每秒收益: JZ_DATA[USER_FILE_NAME]['反物质聚光镜每秒收益'], 反物质聚光镜时间戳: SJC_DATA[USER_FILE_NAME]['反物质聚光镜时间戳'],
                    棱镜: JZ_DATA[USER_FILE_NAME]['棱镜'], 棱镜每秒收益: JZ_DATA[USER_FILE_NAME]['棱镜每秒收益'], 棱镜时间戳: SJC_DATA[USER_FILE_NAME]['棱镜时间戳'],
                    机会制造器: JZ_DATA[USER_FILE_NAME]['机会制造器'], 机会制造器每秒收益: JZ_DATA[USER_FILE_NAME]['机会制造器每秒收益'], 机会制造器时间戳: SJC_DATA[USER_FILE_NAME]['机会制造器时间戳'],
                    分形引擎: JZ_DATA[USER_FILE_NAME]['分形引擎'], 分形引擎每秒收益: JZ_DATA[USER_FILE_NAME]['分形引擎每秒收益'], 分形引擎时间戳: SJC_DATA[USER_FILE_NAME]['分形引擎时间戳'],
                    Javascript控制台: JZ_DATA[USER_FILE_NAME]['Javascript控制台'], Javascript控制台每秒收益: JZ_DATA[USER_FILE_NAME]['Javascript控制台每秒收益'], Javascript控制台时间戳: SJC_DATA[USER_FILE_NAME]['Javascript控制台时间戳'],
                    空闲宇宙: JZ_DATA[USER_FILE_NAME]['空闲宇宙'], 空闲宇宙每秒收益: JZ_DATA[USER_FILE_NAME]['空闲宇宙每秒收益'], 空闲宇宙时间戳: SJC_DATA[USER_FILE_NAME]['空闲宇宙时间戳'],
                    脑皮屑面包师: JZ_DATA[USER_FILE_NAME]['脑皮屑面包师'], 脑皮屑面包师每秒收益: JZ_DATA[USER_FILE_NAME]['脑皮屑面包师每秒收益'], 脑皮屑面包师时间戳: SJC_DATA[USER_FILE_NAME]['脑皮屑面包师时间戳'],
                    你: JZ_DATA[USER_FILE_NAME]['你'], 你每秒收益: JZ_DATA[USER_FILE_NAME]['你每秒收益'], 你时间戳: SJC_DATA[USER_FILE_NAME]['你时间戳']
                },
                UI: {}
            }

            const NEW_USER_FILE = `${NEW_ALL_USER_FOLDER}${USER_FILE_NAME}.json`
            fs.writeFileSync(NEW_USER_FILE, JSON.stringify(USER_DATA, null, 4), 'utf8')
        })
        return false
    }
}