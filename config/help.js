export const helpCfg = {
    title: 'Cookie帮助',
    subTitle: 'Cookie-plugin',
    columnCount: 0,
    colWidth: 265,
    theme: 'all',
    themeExclude: ['default'],
}
export const helpList = [{
    "group": "说明",
    "auth": false,
    "list": [{
        "icon": 0,
        "title": "曲奇说明",
        "desc": "游玩说明书"
    }]
}, {
    "group": "基础",
    "auth": false,
    "list": [{
        "icon": 0,
        "title": "点击曲奇",
        "desc": "新用户创建信息"
    }, {
        "icon": 0,
        "title": "领取收益",
        "desc": "领取建筑产生的收益"
    }, {
        "icon": 0,
        "title": "曲奇统计信息",
        "desc": "查看个人信息统计"
    }]
}, {
    "group": "购买",
    "auth": false,
    "list": [{
        "icon": 0,
        "title": "购置建筑(建筑名称)*数量",
        "desc": "购买建筑"
    }, {
        "icon": 0,
        "title": "购置(建筑名称)升级(升级名称)",
        "desc": "购买建筑升级选项"
    }, {
        "icon": 0,
        "title": "购置天堂升级(升级名称)"
    }]
}, {
    "group": "商店",
    "auth": false,
    "list": [{
        "icon": 0,
        "title": "建筑商店",
        "desc": "查看所有建筑"
    }, {
        "icon": 0,
        "title": "(建筑名称)升级商店",
        "desc": "正在推出...."
    }, {
        "icon": 0,
        "title": "天堂商店",
        "desc": "正在推出...."
    }]
}, {
    "group": "其他",
    "auth": false,
    "list": [{
        "icon": 0,
        "title": "生成兑换码*数量",
        "desc": "生成一份曲奇兑换码"
    }, {
        "icon": 0,
        "title": "兑换曲奇[兑换码]",
        "desc": "兑换曲奇"
    }]
}]