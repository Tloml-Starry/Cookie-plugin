import fs from 'node:fs'

if (!global.segment) {
    global.segment = (await import("oicq")).segment
}

const NEW_ALL_USER_FOLDER = 'plugins/Cookie-plugin/data/USER INFO/'
fs.mkdirSync('data/Ts-GameData', { recursive: true })
fs.mkdirSync(NEW_ALL_USER_FOLDER, { recursive: true })

const dir1 = './plugins/Cookie-plugin/apps';

const files = [
    ...fs.readdirSync(dir1)
].filter(file => file.endsWith('.js'));

let ret = []

logger.info(`曲奇点点乐初始化....`)
files.forEach((file) => {
    ret.push(import(`./apps/${file}`))
})

ret = await Promise.allSettled(ret)

let apps = {}
for (let i in files) {
    let name = files[i].replace('.js', '')

    if (ret[i].status != 'fulfilled') {
        logger.error(`载入插件错误：${logger.red(name)}`)
        logger.error(ret[i].reason)
        continue
    }
    apps[name] = ret[i].value[Object.keys(ret[i].value)[0]]
}
logger.mark(`曲奇点点乐加载成功！感谢您的使用！`)
export { apps }
