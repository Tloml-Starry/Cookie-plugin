// { label: '点击直接发送此内容', enter: true }
// { label: '点击直接发送后面的内容', callback: '发送我' }
// { label: '点击不直接发送后面的内容', data: 'is me' }
// { label: '点击不直接发送此内容' }
// { label: '点击跳转链接', link: '链接' }
export default class Button {
    constructor() {
        this.plugin = {
            name: 'Cookie-plugin',
            dsc: 'Cookie-plugin',
            priority: 1,
            rule: [{
                reg: /^(#|\/)?建筑商店$/,
                fnc: 'CS'
            }, {
                reg: /^(#|\/)?(.*)升级商店$/, // 后续添加
                fnc: 'BU'
            }, {
                reg: /^(#|\/)?(曲奇|Cookie)菜单$/,
                fnc: 'COOKIE_HELP'
            }, {
                reg: /^(#|\/)?领取收益$/,
                fnc: 'C_STATISTICS'
            }]
        }
    }

    CS() {
        return Bot.Button([[
            { label: '指针', data: '/购置建筑指针*1' },
            { label: '老太婆', data: '/购置建筑老太婆*1' },
            { label: '农场', data: '/购置建筑农场*1' },
            { label: '矿坑', data: '/购置建筑矿坑*1' }
        ], [
            { label: '工厂', data: '/购置建筑工厂*1' },
            { label: '银行', data: '/购置建筑银行*1' },
            { label: '神庙', data: '/购置建筑神庙*1' },
            { label: '巫师塔', data: '/购置建筑巫师塔*1' }
        ], [
            { label: '装运飞船', data: '/购置建筑装运飞船*1' },
            { label: '炼金术实验室', data: '/购置建筑炼金术实验室*1' },
            { label: '传送门', data: '/购置建筑传送门*1' },
            { label: '时光机', data: '/购置建筑时光机*1' }
        ], [
            { label: '反物质聚光镜', data: '/购置建筑反物质聚光镜*1' },
            { label: '棱镜', data: '/购置建筑棱镜*1' },
            { label: '机会制造器', data: '/购置建筑机会制造器*1' },
            { label: '分形引擎', data: '/购置建筑分形引擎*1' }
        ], [
            { label: 'Javascript控制台', data: '/购置建筑Javascript控制台*1' },
            { label: '空闲宇宙', data: '/购置建筑空闲宇宙*1' },
            { label: '脑皮屑面包师', data: '/购置建筑脑皮屑面包师*1' },
            { label: '你', data: '/购置建筑你*1' }
        ]])
    }

    BU() {
        return Bot.Button([[
            { label: '该功能暂未完善' }
        ]])
    }

    COOKIE_HELP() {
        return Bot.Button([[
            { label: '曲奇说明', enter: true },
            { label: '点击曲奇', enter: true },
            { label: '领取收益', enter: true },
            { label: '统计信息', callback: '曲奇统计信息' }
        ], [
            { label: '建筑商店', enter: true }
        ]])
    }

    C_STATISTICS() {
        return Bot.Button([[
            { label: '领取收益', enter: true },
            { label: '统计信息', callback: '曲奇统计信息' }
        ], [
            { label: '建筑商店', enter: true }
        ]])
    }
}