import { Convert } from '../utils/Nc.js';
import { GET_DATA, SAVE_DATA } from '../utils/Gs.js';

export function cop(e, ORIGINAL_PRICE, BUILDINGS, QUANTITY_PURCHASE, Brps) {
    const USER_ID = e.user_id;
    const CURRENT_TIME_STAMP = Date.now()

    const USER_FILE = `plugins/Cookie-plugin/data/USER INFO/${USER_ID}.json`
    const USER_DATA = GET_DATA(USER_FILE)

    const CURRENT_NUMBER = USER_DATA['SI']['当前饼干数量']
    const BUILDINGS_NUMBER = USER_DATA['BI'][BUILDINGS]

    let CK_PRICE = ORIGINAL_PRICE;

    let TOTAL_PRICE = 0;

    const rateMap = {
        '指针': 1.2,
        '老太婆': 1.2,
        '农场': 1.3,
        '矿坑': 1.3,
        '工厂': 1.4,
        '银行': 1.4,
        '神庙': 1.5,
        '巫师塔': 1.5,
        '装运飞船': 1.6,
        '炼金术实验室': 1.6,
        '传送门': 1.7,
        '时光机': 1.7,
        '反物质聚光镜': 1.8,
        '棱镜': 1.8,
        '机会制造器': 1.9,
        '分形引擎': 1.9,
        'Javascript控制台': 2,
        '空闲宇宙': 2,
        '脑皮屑面包师': 2.1,
        '你': 2.1,
    };

    const RATE_MULTIPLICATION = rateMap[BUILDINGS]

    if (BUILDINGS_NUMBER === 0) USER_DATA['BI'][`${BUILDINGS}时间戳`] = CURRENT_TIME_STAMP

    if (!USER_DATA['BI'][`${BUILDINGS}时间戳`]) {
        USER_DATA['BI'][`${BUILDINGS}时间戳`] = CURRENT_TIME_STAMP
        SAVE_DATA(USER_FILE, USER_DATA)
    }

    if (BUILDINGS_NUMBER === 0) {
        CK_PRICE *= Math.pow(RATE_MULTIPLICATION, QUANTITY_PURCHASE - 1);
    } else if (BUILDINGS_NUMBER > 0) {
        for (let i = 0; i < QUANTITY_PURCHASE; i++) {
            const currentPrice = CK_PRICE * Math.pow(RATE_MULTIPLICATION, BUILDINGS_NUMBER + i);
            TOTAL_PRICE += currentPrice;
        }
        CK_PRICE = TOTAL_PRICE;
    }

    CK_PRICE = Math.floor(CK_PRICE);


    if (CURRENT_NUMBER < CK_PRICE) {
        return e.reply((e.adapter === 'QQBot') ? [
            '# 曲奇数量不足！',
            '> ', segment.at(USER_ID),
            `需要曲奇：${Convert(CK_PRICE)}`,
            `当前曲奇：${Convert(CURRENT_NUMBER)}`
        ] : [
            segment.at(USER_ID),
            `\n曲奇数量不足！\n需要曲奇：${Convert(CK_PRICE)}\n当前曲奇：${Convert(CURRENT_NUMBER)}`
        ])
    }

    USER_DATA['SI']['当前饼干数量'] -= CK_PRICE;
    USER_DATA['SI']['每秒饼干产量'] += Brps * QUANTITY_PURCHASE;
    USER_DATA['SI']['拥有建筑'] += QUANTITY_PURCHASE;
    USER_DATA['BI'][BUILDINGS] += QUANTITY_PURCHASE;
    USER_DATA['BI'][`${BUILDINGS}每秒收益`] += Brps * QUANTITY_PURCHASE;

    const REVENUE_PER_SECOND = Convert(USER_DATA['BI'][`${BUILDINGS}每秒收益`])
    const LEFTOVER_CK = Convert(USER_DATA['SI']['当前饼干数量'])

    SAVE_DATA(USER_FILE, USER_DATA);

    return e.reply((e.adapter === 'QQBot') ? [
        '# 购置成功！',
        '> ', segment.at(USER_ID),
        `消耗曲奇：${Convert(CK_PRICE)}`,
        `剩余曲奇：${LEFTOVER_CK}`,
        `当前${BUILDINGS}数量：${BUILDINGS_NUMBER + QUANTITY_PURCHASE}`,
        `当前${BUILDINGS}每秒收益：${REVENUE_PER_SECOND}`,
        '此每秒收益不计算升级项'
    ] : [
        segment.at(USER_ID),
        `\n购置成功！\n消耗曲奇：${Convert(CK_PRICE)}\n剩余曲奇：${LEFTOVER_CK}\n当前${BUILDINGS}数量：${BUILDINGS_NUMBER + QUANTITY_PURCHASE}\n当前${BUILDINGS}每秒收益：${REVENUE_PER_SECOND}\n此每秒收益不计算升级项`
    ])
}
