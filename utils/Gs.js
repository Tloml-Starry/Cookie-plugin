import fs from 'fs'

const ALL_USER_FOLDER = 'plugins/Cookie-plugin/data/USER INFO/'

export function GET_DATA(FILE_PATH) {
    const DATA = fs.readFileSync(FILE_PATH, 'utf8')
    const DATA_JSON = JSON.parse(DATA)
    return DATA_JSON
}

export function SAVE_DATA(FILE_PATH, DATA) { fs.writeFileSync(FILE_PATH, JSON.stringify(DATA, null, 4), 'utf8') }

export function IS_THERE_USER(ID) {
    let STATE = true
    if (!fs.existsSync(`${ALL_USER_FOLDER}${ID}.json`)) STATE = false
    return STATE
}