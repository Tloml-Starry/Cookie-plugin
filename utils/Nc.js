export function Convert(value) {
    const units = [
        { name: 'quattuordecillion', power: 48 },
        { name: 'tredecillion', power: 45 },
        { name: 'duodecillion', power: 42 },
        { name: 'undecillion', power: 39 },
        { name: 'decillion', power: 36 },
        { name: 'nonillion', power: 33 },
        { name: 'octillion', power: 30 },
        { name: 'septillion', power: 27 },
        { name: 'sextillion', power: 24 },
        { name: 'quintillion', power: 21 },
        { name: 'quadrillion', power: 18 },
        { name: 'trillion', power: 15 },
        { name: 'billion', power: 12 },
        { name: 'million', power: 6 }
    ];

    let result = value;
    for (let i = 0; i < units.length; i++) {
        if (value >= Math.pow(10, units[i].power)) {
            result = `${(value / Math.pow(10, units[i].power)).toFixed(3)} ${units[i].name}`;
            break;
        }
    }

    return result;
}